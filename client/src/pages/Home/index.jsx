import { Community, JoinOurProgram } from "../../components"


export const Home = () => {
  return (
    <>
    <Community/>
    <JoinOurProgram/>
    </>
  )
}
